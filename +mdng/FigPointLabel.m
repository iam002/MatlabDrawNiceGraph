function an = FigPointLabel(p, str, loc, varargin)
%FigPointLabel - 向figure上的点添加文本说明
% 
% Syntax: 
%   an = FigPointLabel(p, str, loc[, ...
%                     'FigOrAxi', 'fig', ...
%                     'Color', color, ...
%                     'FontSize', fontsize, ...
%                     'FontName', fontname, ...
%                     'FontWeight', 'normal', ...
%                     'FontAngle', 'normal']) 
%
% Params: 
%   - p    [R] [numel=2] 点在figure上的坐标
%   - str  [R] [[char], [string]] 待标注的文本
%   - loc  [R] [char; choices] 文本相对点的位置, 可选值包括：
%           * 'north'
%           * 'west'
%           * 'east'
%           * 'south'
%           * 'northwest'
%           * 'northeast'
%           * 'southwest'
%           * 'southeast'
%           每个选项相对 p 的位置: 
%                northwest,     north,   northeast,
%                     west,       p           east,
%                southwest,     south,   southeast, 
%
%    - FigOrAxi     [N] [choices] 坐标p是相对图层fig还是坐标轴axi
%           * 'fig'
%           * 'axi'
%
%    - Color        [N] [[numeric], [char]] 字体的颜色
%    - FontSize     [N] [numeric] 字体大小
%    - FontName     [N] [[char], [string]] 字体
%    - FontWeight   [N] [choices] 字符粗细
%           * 'normal'
%           * 'bold'
%
%    - FontAngle    [N] [choices] 字符倾斜
%           * 'normal'
%           * 'italic'
% 
% Return: 
%   an TextBox对象
% 
% Matlab Version: R2021b
%
% See also:
%   mdng.DrawLine
%   mdng.CoorFromAxis2Fig

%% 输入参数处理
% 将元胞数组转换为结构体
inputParams = mdng.cellToStruct(varargin);
% 默认参数的结构体
defaultParams = struct( ...
    'FigOrAxi', 'fig', ...
    'Color', [0.8, 0.52, 0.98], ...
    'FontSize',  14, ...
    'FontName', 'FixedWidth', ...
    'FontWeight', 'normal', ...
    'FontAngle', 'normal' ...
    );
% 修改默认设置
defaultParams = mdng.structAssign(defaultParams, inputParams);

%% 在坐标轴上的指定某点添加标注
if strcmp(defaultParams.FigOrAxi, 'axi')
    p = mdng.CoorFromAxis2Fig(gca, p);
end
ptx = p(1); pty = p(2);
switch loc
    case 'north'
        label_pos = [ptx-0.05, pty-0.01, 0.1, 0.1];
        hAlign = 'center';
        vAlign = 'bottom';
    case 'south'
        label_pos = [ptx-0.05, pty-0.1, 0.1, 0.1];
        hAlign = 'center';
        vAlign = 'top';
    case 'east'
        label_pos = [ptx, pty-0.05-0.008, 0.1, 0.1];
        hAlign = 'left';
        vAlign = 'middle';
    case 'west'
        label_pos = [ptx-0.1, pty-0.05-0.008, 0.1, 0.1];
        hAlign = 'right';
        vAlign = 'middle';
    case 'northwest'
        label_pos = [ptx-0.1, pty-0.01, 0.1, 0.1];
        hAlign = 'right';
        vAlign = 'bottom';
    case 'northeast'
        label_pos = [ptx, pty-0.01, 0.1, 0.1];
        hAlign = 'left';
        vAlign = 'bottom';
    case 'southwest'
        label_pos = [ptx-0.1, pty-0.1, 0.1, 0.1];
        hAlign = 'right';
        vAlign = 'top';
    case 'southeast'
        label_pos = [ptx, pty-0.1, 0.1, 0.1];
        hAlign = 'left';
        vAlign = 'top';
    otherwise
        label_pos = [ptx-0.05, pty-0.01, 0.1, 0.1];
        hAlign = 'center';
        vAlign = 'bottom';
end
an = annotation(gcf, 'textbox', ...
        label_pos, ...
        'Color', defaultParams.Color, ...
        'String', str, ...
        'Interpreter', 'latex', ...
        'FontSize', defaultParams.FontSize, ...
        'FontName', defaultParams.FontName, ...
        'FontAngle', defaultParams.FontAngle, ...
        'FontWeight', defaultParams.FontWeight, ...
        'EdgeColor', 'None', ...
        'FitBoxToText', 'On', ...
        'VerticalAlignment', vAlign, ...
        'HorizontalAlignment', hAlign);
end