function line_obj = DrawLine(h_axi, pnt1, pnt2, varargin)
%DrawLine - 在图窗上绘制直线，可设置端口类型和直线类型
%
% Syntax: 
%   line_obj = DrawLine(h_axi, pnt1, pnt2, varargin)
%
% Params: 
%   - h_axi         [R] 坐标轴句柄
%   - pnt1          [R] [numeric; numel=2] 端点1在坐标轴上的坐标
%   - pnt2          [R] [numeric; numel=2] 端点2在坐标轴上的坐标
%   - Type          [N] [choices] 类型,三个可选值:
%       * 'line'
%       * 'arrow'
%       * 'doublearrow'
%
%   - Color         [N] [[numeric; numel=3], [char]] 颜色
%   - LineStyle     [N] [choices] 线型
%       * '-'
%       * '--'
%       * ':'
%       * '-.'
%       * 'none'
%
%   - LineWitdh     [N] [numeric] 线宽
%   - HeadStyle     [N] [choices] 箭头的类型
%       * 'vback2'
%       * 'plain'
%       * 'vback1'
%       * 'cback1'
%
%   - HeadWidth     [N] [numeric] 箭头宽度
%   - HeadLength    [N] [numeric] 箭头长度
%
% Return:
%   line_obj 图形对象
%
% Note: 自动根据直线长度调整箭头大小
%
% Matlab Version: R2021b
%
% See also:
%   mdng.FigPointLabel
%   mdng.DrawAxisWithArrow

%% 输入参数处理
% 将元胞数组转换为结构体
inputParams = mdng.cellToStruct(varargin);
% 默认参数的结构体
defaultParams = struct( ...
    'Type', 'line', ...
    'Color',  [0.8, 0.52, 0.98], ...
    'LineStyle', '-', ...
    'LineWidth', 0.5, ...
    'HeadStyle', 'vback2', ...
    'HeadWidth', 6, ...
    'HeadLength', 10 ...
    );
% 修改默认设置
defaultParams = mdng.structAssign(defaultParams, inputParams);

%%
fpnt1 = mdng.CoorFromAxis2Fig(h_axi, pnt1); 
fpnt2 = mdng.CoorFromAxis2Fig(h_axi, pnt2); 
line_obj = annotation(defaultParams.Type, [fpnt1(1), fpnt2(1)], [fpnt1(2), fpnt2(2)]);
line_obj.LineStyle = defaultParams.LineStyle;
line_obj.LineWidth = defaultParams.LineWidth;
line_obj.Color = defaultParams.Color;

if strcmp(defaultParams.Type, 'arrow')
    line_obj.HeadWidth = defaultParams.HeadWidth;
    line_obj.HeadLength = defaultParams.HeadLength;
    line_obj.HeadStyle = defaultParams.HeadStyle;
end

if strcmp(defaultParams.Type, 'doublearrow')
    line_obj.Head1Width = defaultParams.HeadWidth;
    line_obj.Head2Width = defaultParams.HeadWidth;
    line_obj.Head1Length = defaultParams.HeadLength;
    line_obj.Head2Length = defaultParams.HeadLength;
    line_obj.Head1Style = defaultParams.HeadStyle;
    line_obj.Head2Style = defaultParams.HeadStyle;
end

end
