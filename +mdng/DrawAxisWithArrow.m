function [xarrow, yarrow] = DrawAxisWithArrow(h_axi, varargin)
%DrawAxisWithArrow - 绘制带箭头的坐标轴
%
% Syntax: 
%   [xarrow, yarrow] = DrawAxisWithArrow(h_axi[, ...
%                       'Color', color, ...
%                       'HeadWidth', headwidth, ...
%                       'XLabel', xlabel, ...
%                       'YLabel', ylabel, ...
%                       'Olabel', olabel ...]);
%
% Params: 
%   - h_axi     [R]                              坐标轴句柄
%   - Color     [N] [[numeric; numel=3], [char]] 坐标轴的颜色
%   - HeadWidth [N] [numeric]                    箭头大小
%   - XLabel    [N] [[char], [string]]           x轴的标签(在箭头上方)
%   - YLabel    [N] [[char], [string]]           y轴的标签(在箭头右方)
%   - Olabel    [N] [[char], [string]]           原点的标签(两坐标轴交点的左下角)
%
% Return: 
%   - xarrow x坐标轴对象
%   - yarrow y坐标轴对象
%
% Matlab Version: R2021b
%
% See also:
%   mdng.FigPointLabel
    
%% 输入参数处理
% 将元胞数组转换为结构体
inputParams = mdng.cellToStruct(varargin);
% 默认参数的结构体
defaultParams = struct( ...
    'ArrowColor', [0.7, 0.62, 1], ...
    'HeadWidth',  6, ...
    'XLabel', '', ...
    'YLabel', '', ...
    'OLabel', '', ...
    'LabelColor', [0.8, 0.52, 0.98], ...
    'LabelFontSize', 14 ...
    );
% 修改默认设置
defaultParams = mdng.structAssign(defaultParams, inputParams);

%% 获得坐标原点的位置，
% 若坐标轴的范围不包含坐标原点，则选择最靠近原点的一角作为坐标轴的交点，
% 坐标轴的箭头指向正向
x_range = get(h_axi, 'XLim');
y_range = get(h_axi, 'YLim');
op = [0, 0]; % 默认交点为坐标原点
if x_range(1) > 0
    op(1) = x_range(1);
elseif x_range(2) < 0
    op(1) = x_range(2);
else
    op(1) = 0;
end
if y_range(1) > 0
    op(2) = y_range(1);
elseif y_range(2) < 0
    op(2) = y_range(2);
else
    op(2) = 0;
end
op = mdng.CoorFromAxis2Fig(h_axi, op); % 转换为在fig上的坐标

% 获得axis的坐标
axis_rec = get(h_axi, 'Position'); % axis在fig的坐标(x,y,w,h)
% 获得fig句柄
h_fig = get(h_axi, 'Parent');
% 绘制 x 坐标轴
xarrow_x = [axis_rec(1)-0.05*axis_rec(3), axis_rec(1)+1.05*axis_rec(3)];
xarrow_y = [op(2), op(2)];
xarrow = annotation(h_fig, 'arrow', xarrow_x, xarrow_y);
set(xarrow, 'HeadStyle', 'plain', 'HeadWidth', defaultParams.HeadWidth, 'Color', defaultParams.ArrowColor);
% 绘制 y 坐标轴
yarrow_x = [op(1), op(1)];
yarrow_y = [axis_rec(2)-0.05*axis_rec(4), axis_rec(2)+1.05*axis_rec(4)];
yarrow = annotation(h_fig, 'arrow', yarrow_x, yarrow_y);
set(yarrow, 'HeadStyle', 'plain', 'HeadWidth', defaultParams.HeadWidth, 'Color', defaultParams.ArrowColor);
% 关闭默认的坐标轴
set(h_axi, 'Visible', 'off');

%% 是否显示坐标标签
if defaultParams.XLabel
    XArrowLabel(xarrow, defaultParams.XLabel, ...
        defaultParams.LabelColor, ...
        defaultParams.LabelFontSize);
end
if defaultParams.YLabel
    YArrowLabel(yarrow, defaultParams.YLabel, ...
        defaultParams.LabelColor, ...
        defaultParams.LabelFontSize);
end
if defaultParams.OLabel
    OriginLabel(op, defaultParams.OLabel, ...
        defaultParams.LabelColor, ...
        defaultParams.LabelFontSize);
end

end

function YArrowLabel(arrow, str, color, fontsize)
% 给 y 坐标轴添加标签
    p = [0, 0];
    arrow_x = get(arrow, 'X');
    arrow_y = get(arrow, 'Y');
    p(1) = arrow_x(2);
    p(2) = arrow_y(2);
    
%     color = [0.8, 0.52, 0.98];
%     fontsize = 14;
    
    mdng.FigPointLabel(p, str, 'east', 'Color', color, 'FontSize', fontsize);
end

function XArrowLabel(arrow, str, color, fontsize)
% 给 x 坐标轴添加标签
    p = [0, 0];
    arrow_x = get(arrow, 'X');
    arrow_y = get(arrow, 'Y');
    p(1) = arrow_x(2);
    p(2) = arrow_y(2);
    
%     color = [0.8, 0.52, 0.98];
%     fontsize = 14;
    
    mdng.FigPointLabel(p, str, 'north', 'Color', color, 'FontSize', fontsize);
end

function OriginLabel(p, str, color, fontsize)
% 给 x 坐标轴添加标签
%     color = [0.8, 0.52, 0.98];
%     fontsize = 14;

    mdng.FigPointLabel(p, str, 'southwest', 'Color', color, 'FontSize', fontsize);
end
