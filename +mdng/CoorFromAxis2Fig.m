function [fp] = CoorFromAxis2Fig(h_axi, axiPnt)
%CoorFromAxis2Fig - 将点在 axis 的坐标转换为该点在 figure 上的坐标
%
% Syntax: 
%   [fp] = CoorFromAxis2Fig(h_axi, axiPnt)
% 
% Params: 
%   - h_axi  [R]                    坐标轴句柄
%   - axiPnt [R] [numeric; numel=2] 点在h_axi上的坐标
%
% Return:
%    fp 该点在figure上的坐标
%
% Matlab Version: R2021b
%
% See also:
%   mdng.FigPointLabel

x_range = get(h_axi, 'XLim'); % axis的x轴范围
y_range = get(h_axi, 'YLim'); % axis的y轴范围
rec = get(h_axi, 'Position'); % axis在fig的坐标(x,y,w,h)
fp(1) = (axiPnt(1)-x_range(1))*rec(3) / (x_range(2)-x_range(1)) + rec(1);
fp(2) = (axiPnt(2)-y_range(1))*rec(4) / (y_range(2)-y_range(1)) + rec(2);

end

