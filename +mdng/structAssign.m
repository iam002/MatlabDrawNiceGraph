function [structDst] = structAssign(structDst, structSrc)
%STRUCTASSIGN - 将结构体src的值赋给结构体dst对应的字段
%
% Syntax:  
%   [structDst] = structAssign(structDst, structSrc)
% 
% Params:
%   - structDst [R] [struct] 目标结构体
%   - structSrc [R] [struct] 源结构体
%
% Return:
%   structDst 赋值后的结构体
%
% Matlab Version: R2021b
%
% See also:
%   mdng.cellToStruct

    keys = fieldnames(structSrc); % 获得结构体B的所有字段
    for i = 1:length(keys)
        cur_key = keys{i};
        if isfield(structDst, cur_key)
            % 2017年后支持: structName.(dynamicExpression)
            % dynamicExpression 是一个变量或表达式，返回字符串标量（结构体字段）
            % 类似于 getfield() 和 setfield() 功能
            structDst.(cur_key) = structSrc.(cur_key);
        end
    end
end