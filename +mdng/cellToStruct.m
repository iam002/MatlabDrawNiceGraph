function params = cellToStruct(inputCell)
%CELLTOSTRUCT - 将元胞数组inputCell转化为结构体params
%
% Syntax:
%   params = cellToStruct(inputCell)
% 
% Params:
%   - inputCell [R] [cell] 通常为 varargin
%
% Return:
%   params 与元胞数组相对应的结构体
%
% Matlab Version: R2021b
%
% Example:
%   >> params = cellToStruct('name', 'iam002', 'age', 18)
%   params = 
%     
%       包含以下字段的 struct:
%     
%         name: 'iam002'
%          age: 18
%
% See also:
%   mdng.structAssign

    N = length(inputCell);
    assert(~bitand(N,1));
    
    params = struct('author', 'wreng'); % 得先初始化个结构体, 不然报错
    for i = 1:2:N
        params.(inputCell{i}) = inputCell{i+1};
    end
end