clear
% 序列的周期延扩

%% 设置fig背景为深色模式
back_color = [0.15, 0.15, 0.15];
h_f = figure('Color', back_color);

%% 创建 axes
% h_a = axes('Parent', h_f);
% set(h_a, 'NextPlot', 'add'); % 相当于 hold on
h_1 = subplot(211);
h_2 = subplot(212);

%% 绘制
x = -4:4;
y = cos(pi/8*x) + 1; %16 

stem(h_1, x, y, 'Color', [0.39, 0.83, 0.07], 'Marker', '.');
xlim(h_1, [-26, 25])
ylim(h_1, [0, 2.5]);

mdng.DrawAxisWithArrow(h_1, ...
    'XLabel', '$n$', 'YLabel', '$x[n]$', 'OLabel', '$O$');

mdng.FigPointLabel( ...
    mdng.CoorFromAxis2Fig(h_1, [-4.5,0]), '$-N_1$', 'south', ...
    'Color', [0,1,1], 'FontSize', 10);
mdng.FigPointLabel( ...
    mdng.CoorFromAxis2Fig(h_1, [4,0]), '$N_1$', 'south', ...
    'Color', [0,1,1], 'FontSize', 10);


y2 = zeros(1,16);
y2(4:12) = y;
y3 = repmat(y2, 1, 3);
stem(h_2, -23:24, y3, 'Color', [0.39, 0.83, 0.07], 'Marker', '.');

xlim(h_2, [-26, 25])
ylim(h_2, [0, 2.5]);

% 绘制一个箭头
mdng.DrawLine(h_2, [0, 0.8], [16, 0.8], ...
    'Type', 'doublearrow', 'Color', [0, 1, 1]);

mdng.FigPointLabel(mdng.CoorFromAxis2Fig(h_2, [8,0.8]), '$N$', 'south', ...
    'Color', [0,1,1]);
mdng.FigPointLabel(mdng.CoorFromAxis2Fig(h_2, [24,0.8]), '$\cdots$', 'south', ...
    'Color', [0.39, 0.83, 0.07]);
mdng.FigPointLabel(mdng.CoorFromAxis2Fig(h_2, [-25,0.8]), '$\cdots$', 'south', ...
    'Color', [0.39, 0.83, 0.07]);
mdng.FigPointLabel(mdng.CoorFromAxis2Fig(h_2, [-4.5,0]), '$-N_1$', 'south', ...
    'Color', [0,1,1], 'FontSize', 10);
mdng.FigPointLabel(mdng.CoorFromAxis2Fig(h_2, [4,0]), '$N_1$', 'south', ...
    'Color', [0,1,1], 'FontSize', 10);


mdng.DrawAxisWithArrow(h_2, ...
    'XLabel', '$n$', 'YLabel', '$\tilde{x}[n]$', 'OLabel', '$O$');

%% 保存图像
if false
    img_name = SaveFig2Img(h_f, './figures/demo04.png');
    fprintf('图像保存为: %s\n', img_name);
end