# MatlabDrawNiceGraph

### 介绍

本仓库对 Matlab 相关的绘图/标注函数进行一定的封装（`+mdng`），以简化 Matlab 部分绘图工作，目前实现的功能有：

- 坐标转换 [mdng.CoorFromAxis2Fig](./+mdng/CoorFromAxis2Fig.m)
  进行坐标转换，将点在坐标轴(axis)上的坐标转换为在图窗(figure)的归一化坐标。
- 文字标注 [mdng.FigPointLabel](./+dng/FigPointLabel].m)
  在图窗上的某点附近添加文字。结合函数 [mdng.CoorFromAxis2Fig](./+mdng/CoorFromAxis2Fig.m)，可以很方便在 axis 上添加文字（只需知道该点在 axis 的坐标）。
- 绘制带箭头的坐标系 [mdng.DrawAxisWithArrow](./+dng/DrawAxisWithArrow.m)
  **自动确定**坐标原点在图窗的位置，并以此来绘制带箭头的坐标系。
- 绘制直线、单向箭头和双向箭头 [mdng.DrawLine](./+mdng/DrawLine.m)

> 参考 matlab 可选参数的设计，函数支持使用 `Key-Value` 对来修改对应字段的默认值，例如：
>
> ```matlab
> mdng.DrawAxisWithArrow(gca, 'XLabel', '$\hat{x}$'); 
> ```
>
> 指定 x 轴的标签，y 轴等未设置的字段均使用默认值。

此外, 添加了函数签名文件 `functionSignatures.json` 支持编辑器内的代码提示. 此文件由程序自动生成 (对代码注释有一定的格式要求，具体见 [https://gitee.com/iam002/generate-function-signatures-json](https://gitee.com/iam002/generate-function-signatures-json)).

![](figures/functionTip1.gif)

![](figures/functionTip2.gif)



### 运行

如果想要在自己的工程中运行，需要：

1. 将包 `+mdng` 复制到你的工程目录下，调用包内的函数需加前缀 `mdng.`
2. 添加当前工程目录添加到 Matlab 的搜索目录 (推荐)

### 实例

1. 绘制带箭头的坐标系，见 [demo01.m](https://gitee.com/iam002/MatlabDrawNiceGraph/blob/main/demo01.m)

   ```matlab
   figure('color', [0.15, 0.15, 0.15]);
   x = linspace(-3, 4, 100);
   y = x.^2;
   plot(x, y, 'Color', [0,1,1], 'LineWidth', 1.5);
   mdng.DrawAxisWithArrow(gca, 'XLabel', '$x$', 'YLabel', '$y$', 'OLabel', '$O$');
   ```

   <img src="./figures/demo01.png" style="zoom:50%;" />

2. 绘制两个带箭头的坐标系，见 [demo02.m](https://gitee.com/iam002/MatlabDrawNiceGraph/blob/main/demo02.m)
	
	<img src="./figures/demo02.png" style="zoom:50%;" />

3. 稍微复杂的例子，见 [demo03.m](https://gitee.com/iam002/MatlabDrawNiceGraph/blob/main/demo03.m)
	
	<img src="./figures/demo03.png" style="zoom:50%;" />
   
4. 绘制双箭头，见 [demo4.m](https://gitee.com/iam002/MatlabDrawNiceGraph/blob/main/demo04.m)
	
	<img src="./figures/demo04.png" style="zoom:50%;" />

## Todo

- [x] 新增函数签名文件 `functionSignatures.json` 支持编辑器内的代码提示.
	